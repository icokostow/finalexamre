package com.telerikacademy.finalproject.pages;

import static com.telerikacademy.finalproject.utils.Utils.getUIMappingByKey;

public class UserPage extends NavigationPage {
    LoginPage loginPage = new LoginPage();
    NavigationPage navigationPage = new NavigationPage();

    private final String FEW_WORDS_FOR_ME = getUIMappingByKey("user.updateText");
    private final String SEARCH_USER = "Alex";
    private final String USER_NAME = "Luk";
    private final String SEARCH_SKILL = "plumber";

    public void userEditHisProfile() {
        loginPage.userCanLogInWeAreSocialNetwork(LoginPage.USERNAME, LoginPage.PASSWORD);
        actions.waitForElementPresentTillTimeout("user.personalProfileBtn", 40);
        actions.clickElement("user.personalProfileBtn");
        actions.waitForElementPresentTillTimeout("user.editProfileBtn", 30);
        actions.clickElement("user.editProfileBtn");
        actions.waitForElementPresentTillTimeout("user.textAreaWordsYourself", 30);
        actions.typeValueInField(FEW_WORDS_FOR_ME, "user.textAreaWordsYourself");
        actions.waitFor(30);
        actions.clickElement("user.updateMyProfileBtn");
        actions.waitFor(50);
        actions.clickElement("user.personalProfileBtn");
    }

    public void userCanSendAcceptConnectionReqestAndDisconnectFromUser() {
        loginPage.userCanLogInWeAreSocialNetwork(LoginPage.USERNAME, LoginPage.PASSWORD);
        actions.waitForElementPresentTillTimeout("user.searchUserField", 50);
        actions.typeValueInField(SEARCH_USER, "user.searchUserField");
        actions.waitFor(20);
        actions.clickElement("searchBtn");
        actions.waitForElementPresentTillTimeout("user.seeProfileUserBtn", 40);
        actions.clickElement("user.seeProfileUserBtn");
        actions.waitForElementPresentTillTimeout("user.connectBtn", 40);
        actions.clickElement("user.connectBtn");

        assertUserSendConnectionRequest();

        loginPage.logOutFromWeAreSocialNetwork();
        loginPage.userCanLogInWeAreSocialNetwork(LoginPage.USERNAME_USER2, LoginPage.PASSWORD_USER2);
        actions.waitForElementPresentTillTimeout("user.personalProfileBtn", 50);
        actions.clickElement("user.personalProfileBtn");
        actions.waitForElementPresentTillTimeout("user.newFriendRequestBtn", 40);
        actions.clickElement("user.newFriendRequestBtn");
        actions.waitForElementPresentTillTimeout("submitBtn", 30);
        actions.clickElement("submitBtn");

        assertUserAcceptedFriendReqest();

        actions.waitForElementPresentTillTimeout("navigation.Home", 40);
        actions.clickElement("navigation.Home");
        actions.waitForElementPresentTillTimeout("user.searchFieldName", 30);
        actions.typeValueInField(USER_NAME, "user.searchFieldName");
        actions.waitFor(10);
        actions.clickElement("searchBtn");
        actions.waitForElementPresentTillTimeout("user.seeProfileUser1Btn", 40);
        actions.clickElement("user.seeProfileUser1Btn");
        actions.waitForElementPresentTillTimeout("submitBtn", 40);
        actions.clickElement("submitBtn");
    }

    public void unregisteredUserCantReadPrivatePost() {
        navigationPage.navigateToPage();
        actions.waitForElementPresentTillTimeout("post.latestPostBtn", 40);
        actions.clickElement("post.latestPostBtn");
        actions.waitFor(50);
        actions.clickHiddenElement("post.showAllPostBtn");
        actions.waitFor(10);
        actions.clickElement("submitBtn");
        actions.waitFor(40);
    }

    public void unregisteredUserCanSearchUsers() {
        navigationPage.navigateToPage();
        actions.waitForElementPresentTillTimeout("user.searchFieldName", 50);
        actions.typeValueInField(SEARCH_USER, "user.searchFieldName");
        actions.waitFor(10);
        actions.clickElement("searchBtn");

        assertUnregisteredUserCanFindRegisteredUserByName();

        actions.waitForElementPresentTillTimeout("navigation.Home", 30);
        actions.clickElement("navigation.Home");
        actions.waitForElementPresentTillTimeout("user.searchFieldSkill", 40);
        actions.typeValueInField(SEARCH_SKILL, "user.searchFieldSkill");
        actions.waitFor(5);
        actions.clickElement("searchBtn");
    }


    //--------------ASSERTS-----------------

    public void assertUserChangedHisProfile() {
        actions.assertElementPresentAfterWait("user.assertEditText");
    }

    public void assertUserSendConnectionRequest() {
        actions.assertElementPresentAfterWait("user.assertRequestSendMsg");
    }

    public void assertUserAcceptedFriendReqest() {
        actions.assertElementPresentAfterWait("user.assertNoConnectionReqest");
    }

    public void assertUsersAreDisconnected() {
        actions.assertElementPresentAfterWait("user.connectBtn");
    }

    public void assertHiddenPostIsNotShownUnregisteredUser() {
        actions.assertElementIsNotPresent("post.allPostField", "anyone see");
    }

    public void assertUnregisteredUserCanFindRegisteredUserByName() {
        actions.waitForElementPresentTillTimeout("user.searchedUserShow", 50);
    }

    public void assertUnregisteredUserCanFindRegisteredUserBySkill() {
        actions.waitForElementPresentTillTimeout("user.searchedSkillShow", 40);
    }


}
