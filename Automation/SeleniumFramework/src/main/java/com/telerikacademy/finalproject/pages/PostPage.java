package com.telerikacademy.finalproject.pages;

import static com.telerikacademy.finalproject.utils.Utils.getUIMappingByKey;

public class PostPage extends NavigationPage {
    LoginPage loginPage = new LoginPage();

    private final String POST_TEXT = getUIMappingByKey("post.typeText");
    private final String POST_COMMENT = getUIMappingByKey("post.commentText");
    private final String POST_EDIT_TEXT = getUIMappingByKey("post.editText");
    private final String POST_TEXT_DEL = getUIMappingByKey("post.textForDeleting");
    private final String POST_COMMENT_DEL = getUIMappingByKey("comment.textForDeleteUser");

    public void registeredUserWriteNewPost() {
        loginPage.userCanLogInWeAreSocialNetwork(LoginPage.USERNAME, LoginPage.PASSWORD);
        actions.waitForElementPresentTillTimeout("post.addNewPostBtn", 10);
        actions.clickElement("post.addNewPostBtn");
        actions.waitForElementPresentTillTimeout("post.visibilityDrop", 10);
        actions.clickElement("post.visibilityDrop");
        actions.clickElement("post.visibilityPublic");
        actions.typeValueInField(POST_TEXT, "post.textField");
        actions.typeValueInField(filePath + "yoda.png", "post.uploadImage");
        actions.waitFor(5);
        actions.clickElement("submitBtn");
    }

    public void registeredUserCommentPost() {
        loginPage.userCanLogInWeAreSocialNetwork(LoginPage.USERNAME, LoginPage.PASSWORD);
        actions.waitForElementPresentTillTimeout("post.latestPostBtn", 10);
        actions.clickElement("post.latestPostBtn");
        actions.waitForElementPresentTillTimeout("post.assertCreated", 10);
        actions.clickElement("post.exploreBtn");
        actions.waitFor(10);
        actions.typeValueInField(POST_COMMENT, "post.textField");
        actions.clickElement("post.postCommentBtn");
    }

    public void registeredUserCanDeleteHisPost() {
        loginPage.userCanLogInWeAreSocialNetwork(LoginPage.USERNAME, LoginPage.PASSWORD);
        actions.waitForElementPresentTillTimeout("post.latestPostBtn", 10);
        actions.clickElement("post.latestPostBtn");
        actions.waitForElementPresentTillTimeout("post.forDeletionAssert", 10);
        actions.clickElement("post.deleteExploreBtn");
        actions.waitForElementPresentTillTimeout("deletePostBtn", 10);
        actions.clickElement("deletePostBtn");
        actions.waitFor(10);
        actions.clickElement("deletePostChooseTrue");
        actions.waitFor(5);
        actions.clickElement("submitBtn");
    }

    public void registeredUserCanEditHisPost() {
        loginPage.userCanLogInWeAreSocialNetwork(LoginPage.USERNAME, LoginPage.PASSWORD);
        actions.waitForElementPresentTillTimeout("post.latestPostBtn", 30);
        actions.clickElement("post.latestPostBtn");
        actions.waitForElementPresentTillTimeout("post.editExploreBtn", 30);
        actions.clickElement("post.editExploreBtn");
        actions.waitForElementPresentTillTimeout("post.editPostBtn", 30);
        actions.clickElement("post.editPostBtn");
        actions.waitFor(30);
        actions.clickElement("post.visibilityPublic");
        actions.typeValueInField(POST_EDIT_TEXT, "post.textField");
        actions.waitFor(10);
        actions.clickElement("submitBtn");
    }

    public void registeredUserCanDeleteHisComment() {
        loginPage.userCanLogInWeAreSocialNetwork(LoginPage.USERNAME, LoginPage.PASSWORD);
        actions.waitForElementPresentTillTimeout("post.latestPostBtn", 30);
        actions.clickElement("post.latestPostBtn");
        actions.waitForElementPresentTillTimeout("post.assertCreated", 30);
        actions.clickElement("post.exploreBtn");
        actions.waitForElementPresentTillTimeout("comment.showCommentBtn", 30);
        actions.clickElement("comment.showCommentBtn");
        actions.waitFor(120);
        actions.clickHiddenElement("comment.DeleteBtn");
        actions.waitFor(40);
        actions.clickElement("comment.confirmDeleteBtn");
        actions.waitForElementPresentTillTimeout("submitBtn", 30);
        actions.clickElement("submitBtn");
    }

    public void registeredUserCanLikeOrDislikeUserPost() {
        loginPage.userCanLogInWeAreSocialNetwork(LoginPage.USERNAME, LoginPage.PASSWORD);
        actions.waitForElementPresentTillTimeout("post.latestPostBtn", 30);
        actions.clickElement("post.latestPostBtn");
        actions.waitForElementPresentTillTimeout("post.assertCreated", 30);
        actions.clickElement("post.likeBtn");

        assertUserLikedPost();

        actions.waitForElementPresentTillTimeout("post.dislikeBtn", 40);
        actions.clickElement("post.dislikeBtn");

    }

    //--------------ASSERTS-----------------
    public void assertPostWasCreated() {
        actions.assertElementPresentAfterWait("post.assertCreated");
    }

    public void assertCommentWasCreated() {
        actions.assertElementPresentAfterWait("post.assertComentCreated");
    }

    public void assertPostWasDeleted() {
        actions.assertElementPresentAfterWait("post.confirmDeleteMsg");
    }

    public void assertPostWasEdited() {
        actions.assertElementPresentAfterWait("post.assertEdited");
    }

    public void assertCommentWasDeleted() {
        actions.assertElementPresentAfterWait("comment.deleteAssertion");
    }

    public void assertUserLikedPost() {
        actions.assertElementPresentAfterWait("post.dislikeBtn");
    }

    public void assertUserDislikedPost() {
        actions.assertElementPresentAfterWait("post.likeBtn");
    }


    //--------------POST AND COMMENT FOR DELETION-----------------
    public void writePostТоDeletе() {
        loginPage.userCanLogInWeAreSocialNetwork(LoginPage.USERNAME, LoginPage.PASSWORD);
        actions.waitForElementPresentTillTimeout("navigation.Home", 40);
        actions.clickElement("navigation.Home");
        actions.waitForElementPresentTillTimeout("post.addNewPostBtn", 50);
        actions.clickElement("post.addNewPostBtn");
        actions.waitForElementPresentTillTimeout("post.visibilityDrop", 30);
        actions.clickElement("post.visibilityDrop");
        actions.clickElement("post.visibilityPublic");
        actions.typeValueInField(POST_TEXT_DEL, "post.textField");
        actions.waitFor(5);
        actions.clickElement("submitBtn");
        actions.waitForElementPresentTillTimeout("navigation.Home", 40);
        actions.clickElement("navigation.Home");
    }

    public void writeCommentТоDeletе() {
        actions.waitForElementPresentTillTimeout("post.latestPostBtn", 30);
        actions.clickElement("post.latestPostBtn");
        actions.waitForElementPresentTillTimeout("post.assertCreated", 30);
        actions.clickElement("post.exploreBtn");
        actions.waitFor(10);
        actions.typeValueInField(POST_COMMENT_DEL, "post.textField");
        actions.clickElement("post.postCommentBtn");
        actions.waitForElementPresentTillTimeout("navigation.Home", 40);
        actions.clickElement("navigation.Home");
    }
}