package com.telerikacademy.finalproject.utils;

import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static org.junit.Assert.assertTrue;

public class UserActions {

    public WebDriver getDriver() {
        return driver;
    }

    final WebDriver driver;

    public UserActions() {
        this.driver = Utils.getWebDriver();
    }

    public static void loadBrowser(String baseUrlKey) {
        Utils.getWebDriver().get(Utils.getConfigPropertyByKey(baseUrlKey));
    }

    public static void quitDriver() {
        Utils.tearDownWebDriver();
    }

    //############# ELEMENT OPERATIONS #########

    public void clickElement(String key, Object... arguments) {
        String locator = Utils.getUIMappingByKey(key, arguments);

        Utils.LOG.info("Clicking on element " + key);
        WebElement element = driver.findElement(By.xpath(locator));
        element.click();
    }

    public void clickHiddenElement(String key, Object... arguments) {
        String locator = Utils.getUIMappingByKey(key, arguments);
        WebElement ele = driver.findElement(By.xpath(locator));
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", ele);
    }

    public void typeValueInField(String value, String field, Object... fieldArguments) {
        String locator = Utils.getUIMappingByKey(field, fieldArguments);
        WebElement element = driver.findElement(By.xpath(locator));
        element.sendKeys(value);
    }

    public void uploadPic(String key, Object... arguments) {
        WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey("upload.Pic")));
        String locator = Utils.getUIMappingByKey(key, arguments);
        //enter the file path onto the file-selection input field
        element.sendKeys(locator);
    }

    //############# WAITS #########

    public boolean waitForElementPresentTillTimeout(String locator, int timeout, Object... arguments) {
        try {
            String xpath = Utils.getUIMappingByKey(locator, arguments);
            Integer defaultTimeout = Integer.parseInt(Utils.getConfigPropertyByKey("config.defaultTimeoutSeconds"));
            WebDriverWait wait = new WebDriverWait(driver, timeout);
            wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(xpath)));
            return true;
        } catch (Exception exception) {
            return false;
        }
    }

    public void waitFor(long timeOutMilliseconds) {
        try {
            Thread.sleep(timeOutMilliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //############# ASSERTS #########
    public void assertPageNavigated(String url) {
        String currentUrl = driver.getCurrentUrl();
        Assert.assertTrue("Landed URL is not as expected. Actual URL: " + currentUrl
                + ". Expected URL: " + url, currentUrl.contains(url));
    }

    public void assertElementPresent(String locator) {
        Utils.LOG.info("Asserting element is present");
        Assert.assertNotNull(driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))));
    }

    public void assertElementIsNotPresent(String locator, String containsText) {
        String text = Utils.getUIMappingByKey(containsText);
        String element = driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))).getText();
        Assert.assertFalse(element.contains(text));
    }

    public void assertElementPresentAfterWait(String locator) {
        waitForElementPresentTillTimeout(locator, 10);
        assertElementPresent(locator);
    }
}
