package com.telerikacademy.finalproject.pages;

public class LoginPage extends NavigationPage {
    public final static String USERNAME = "darthvaider";
    public final static String PASSWORD = "telqa123$";
    public final static String USERNAME_USER2 = "Alexico";
    public final static String PASSWORD_USER2 = "telqa123$";
    public final static String ADMIN_USERNAME = "ucakost";
    public final static String ADMIN_PASSWORD = "telqa123$";

    public void userCanLogInWeAreSocialNetwork(String username, String password) {
        actions.waitForElementPresentTillTimeout("log.signInBtn", 40);
        actions.clickElement("log.signInBtn");
        actions.waitForElementPresentTillTimeout("log.usernameField", 40);
        actions.typeValueInField(username, "log.usernameField");
        actions.typeValueInField(password, "log.passwordField");
        actions.clickElement("submitBtn");
        actions.waitFor(20);
    }

    public void logOutFromWeAreSocialNetwork() {
        actions.waitForElementPresentTillTimeout("navigation.Home", 30);
        actions.clickElement("navigation.Home");
        actions.waitForElementPresentTillTimeout("logOutBtn", 30);
        actions.clickElement("logOutBtn");
        actions.assertElementPresentAfterWait("navigation.Home");
        actions.clickElement("navigation.Home");
    }

    //--------------ASSERTS-----------------
    public void assertUserIsLoggedIn() {
        actions.assertElementPresentAfterWait("user.personalProfileBtn");
    }

    public void assertAdminIsLoggedIn() {
        actions.assertElementPresentAfterWait("admin.adminZoneBtn");
    }
}
