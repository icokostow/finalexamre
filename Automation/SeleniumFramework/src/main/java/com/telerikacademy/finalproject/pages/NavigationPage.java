package com.telerikacademy.finalproject.pages;

public class NavigationPage extends BasePage {

    private final String baseurl = "https://mighty-tundra-55013.herokuapp.com/";

    public NavigationPage() {
        super("base.url");
    }


    //--------------ASSERTS-----------------
    public void assertLoginPageNavigated() {
        actions.assertPageNavigated(baseurl);
    }
}