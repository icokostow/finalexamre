package com.telerikacademy.finalproject.pages;

import static com.telerikacademy.finalproject.utils.Utils.getUIMappingByKey;

public class AdminPage extends NavigationPage{
    LoginPage loginPage = new LoginPage();

    public final String POST_TEXT_DEL_ADMIN = getUIMappingByKey("post.textForAdminDelete");
    public final String POST_COMMENT_DEL_ADMIN = getUIMappingByKey("comment.textToDeleteAdmin");


    public void adminCanDeleteUserPost(){
        loginPage.userCanLogInWeAreSocialNetwork(LoginPage.ADMIN_USERNAME, LoginPage.ADMIN_PASSWORD);
        actions.waitForElementPresentTillTimeout("post.latestPostBtn", 30);
        actions.clickElement("post.latestPostBtn");
        actions.waitForElementPresentTillTimeout("admin.browsePostBtn", 30);
        actions.clickElement("admin.browsePostBtn");
        actions.waitForElementPresentTillTimeout("admin.assertThereIsAPost", 30);
        actions.clickElement("admin.explorePostToDelete");
        actions.waitForElementPresentTillTimeout("deletePostBtn", 30);
        actions.clickElement("deletePostBtn");
        actions.waitFor(20);
        actions.clickElement("deletePostChooseTrue");
        actions.waitFor(10);
        actions.clickElement("submitBtn");
    }

    public void adminCanDeleteUserComment(){
        loginPage.userCanLogInWeAreSocialNetwork(LoginPage.ADMIN_USERNAME, LoginPage.ADMIN_PASSWORD);
        actions.waitForElementPresentTillTimeout("post.latestPostBtn", 30);
        actions.clickElement("post.latestPostBtn");
        actions.waitForElementPresentTillTimeout("admin.browsePostBtn", 30);
        actions.clickElement("admin.browsePostBtn");
        actions.waitForElementPresentTillTimeout("post.assertCreated", 30);
        actions.clickElement("post.exploreBtn");
        actions.waitForElementPresentTillTimeout("comment.showCommentBtn", 30);
        actions.clickElement("comment.showCommentBtn");
        actions.waitFor(100);
        actions.clickHiddenElement("admin.commentDeleteBtn");
        actions.waitFor(30);
        actions.clickElement("comment.confirmDeleteBtn");
        actions.waitForElementPresentTillTimeout("submitBtn",30);
        actions.clickElement("submitBtn");
    }

    //--------------ASSERTS-----------------

    public void assertPostWasDeletedFromAdmin(){
        actions.assertElementPresentAfterWait("post.confirmDeleteMsg");
    }
    public void assertCommentWasDeletedFromAdmin(){
        actions.assertElementPresentAfterWait("comment.deleteAssertion");
    }

    //--------------POST AND COMMENT FOR DELETION-----------------

    public void writePostToDeleteFromAdmin() {
        loginPage.userCanLogInWeAreSocialNetwork(LoginPage.USERNAME, LoginPage.PASSWORD);
        actions.waitForElementPresentTillTimeout("navigation.Home", 30);
        actions.clickElement("navigation.Home");
        actions.waitForElementPresentTillTimeout("post.addNewPostBtn", 50);
        actions.clickElement("post.addNewPostBtn");
        actions.waitForElementPresentTillTimeout("post.visibilityDrop", 30);
        actions.clickElement("post.visibilityDrop");
        actions.clickElement("post.visibilityPublic");
        actions.typeValueInField(POST_TEXT_DEL_ADMIN, "post.textField");
        actions.waitFor(5);
        actions.clickElement("submitBtn");
        actions.waitForElementPresentTillTimeout("navigation.Home", 40);
        actions.clickElement("navigation.Home");
    }
    public void writeCommentТоDeletеFromAdmin(){
        actions.waitForElementPresentTillTimeout("post.latestPostBtn", 30);
        actions.clickElement("post.latestPostBtn");
        actions.waitForElementPresentTillTimeout("post.assertCreated", 40);
        actions.clickElement("post.exploreBtn");
        actions.waitForElementPresentTillTimeout("post.textField",70);
        actions.typeValueInField(POST_COMMENT_DEL_ADMIN,"post.textField");
        actions.clickElement("post.postCommentBtn");
        actions.waitForElementPresentTillTimeout("navigation.Home", 40);
        actions.clickElement("navigation.Home");
    }

}
