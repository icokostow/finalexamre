Meta:
@LogIN

Narrative:
As a user
I want to log in WeAre Social Network
So that I can read latest posts

Scenario: LogIn scenario in social network
Given Click log.signInBtn element
When Wait for log.usernameField, 8 seconds
And Type ucakost in log.usernameField field
And Type telqa123$ in log.passwordField field
And Click submitBtn element
Then Assert that user.personalProfileBtn present

Given Click logOutBtn element
When Wait for navigation.Home, 8 seconds
Then Click navigation.Home element