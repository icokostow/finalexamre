Meta:
@RegisterUserWithSameUsername

Narrative:
As a client
I want to try to register in WeAre Social Network
But with already registered username

Scenario: Unregistered user try to register in WeAre social network with already registered username
Given Click log.registerUserBtn element
When Wait for log.registerHeader, 8 seconds
And Type darthvaider in log.newUserNameField field
And Type tonyjordanova@abv.bg in log.newEmailField field
And Type 1234567 in log.newPasswordField field
And Type 1234567 in log.newPasswordConfirmField field
And Click on log.newSkill hidden button
And Wait for, 5 seconds
And Click submitBtn element
And Wait for, 7 seconds
Then Assert that log.newUserWithSameUsernameWrongMsg present

Then Click navigation.Home element