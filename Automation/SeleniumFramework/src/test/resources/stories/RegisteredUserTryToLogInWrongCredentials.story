Meta:
@RegisteredUserWrongCredentials

Narrative:
As a user
I want to log in WeAre Social Network
So that I can read latest posts

Scenario: Registered user try to log in WeAre social network with wrong credentials
Given Click log.signInBtn element
When Wait for log.usernameField, 8 seconds
And Type darthvaider in log.usernameField field
And Type R2D2 in log.passwordField field
And Click submitBtn element
And Wait for, 5 seconds
Then Assert that wrongUsernameMsg present

Then Click navigation.Home element