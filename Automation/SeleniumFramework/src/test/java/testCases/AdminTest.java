package testCases;

import categoriesForGroupTesting.Admin;
import categoriesForGroupTesting.CriticalPriority;
import com.telerikacademy.finalproject.pages.LoginPage;
import org.junit.After;
import org.junit.Test;
import org.junit.experimental.categories.Category;

public class AdminTest extends BaseTest {

    @After
    public void logOutFromWeAreSocialNetwork() {
        loginPage.logOutFromWeAreSocialNetwork();
    }

    @Test
    @Category({CriticalPriority.class, Admin.class})
    public void adminCanLogInWeAreSocialNetwork() {
        loginPage.userCanLogInWeAreSocialNetwork(LoginPage.ADMIN_USERNAME, LoginPage.ADMIN_PASSWORD);
        loginPage.assertAdminIsLoggedIn();
    }
}
