package testCases;

import categoriesForGroupTesting.Admin;
import categoriesForGroupTesting.CriticalPriority;
import com.telerikacademy.finalproject.pages.AdminPage;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;

public class AdminDeleteTest extends BaseTest {

    @AfterClass
    public static void makePostForDeletionAndLogOut() {
        AdminPage adminPage = new AdminPage();
        adminPage.writePostToDeleteFromAdmin();
        adminPage.writeCommentТоDeletеFromAdmin();
    }

    @After
    public void logOutFromWeAreSocialNetwork() {
        loginPage.logOutFromWeAreSocialNetwork();
    }

    @Test
    @Category({CriticalPriority.class, Admin.class})
    public void adminCanDeleteUserPosts() {
        adminPage.adminCanDeleteUserPost();
        adminPage.assertPostWasDeletedFromAdmin();
    }

    @Test
    @Category({CriticalPriority.class, Admin.class})
    public void adminCanDeleteUserComment() {
        adminPage.adminCanDeleteUserComment();
        adminPage.assertCommentWasDeletedFromAdmin();
    }
}
