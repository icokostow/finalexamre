package testCases;

import categoriesForGroupTesting.HighPriority;
import categoriesForGroupTesting.MediumPriority;
import org.junit.After;
import org.junit.Test;
import org.junit.experimental.categories.Category;

public class UnregisteredUserTest extends BaseTest {

    @Test
    @Category(HighPriority.class)
    public void unregisteredUserCantReadPrivatePost() {
        userPage.unregisteredUserCantReadPrivatePost();
        userPage.assertHiddenPostIsNotShownUnregisteredUser();
    }

    @Test
    @Category(MediumPriority.class)
    public void unregisteredUserCanSearchRegisteredUserBySkillsOrName() {
        userPage.unregisteredUserCanSearchUsers();
        userPage.assertUnregisteredUserCanFindRegisteredUserBySkill();
    }

    @After
    public void goHomePage() {
        navigationPage.navigateToPage();
    }
}
