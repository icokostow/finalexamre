package testCases;

import com.telerikacademy.finalproject.pages.*;
import com.telerikacademy.finalproject.utils.UserActions;
import org.junit.AfterClass;
import org.junit.BeforeClass;

public class BaseTest extends NavigationPage {
    PostPage postPage = new PostPage();
    LoginPage loginPage = new LoginPage();
    NavigationPage navigationPage = new NavigationPage();
    UserPage userPage = new UserPage();
    AdminPage adminPage = new AdminPage();

    @BeforeClass
    public static void setUp() {
        UserActions.loadBrowser("base.url");
    }

    @AfterClass
    public static void tearDown() {
        UserActions.quitDriver();
    }
}
