package testCases;

import categoriesForGroupTesting.CriticalPriority;
import categoriesForGroupTesting.MediumPriority;
import categoriesForGroupTesting.RegisteredUser;
import org.junit.After;
import org.junit.Test;
import org.junit.experimental.categories.Category;

public class PostTest extends BaseTest {

    @After
    public void logOutFromWeAreSocialNetwork() {
        loginPage.logOutFromWeAreSocialNetwork();
    }

    @Test
    @Category({CriticalPriority.class, RegisteredUser.class})
    public void registerUserCanWriteAPost() {
        postPage.registeredUserWriteNewPost();
        postPage.assertPostWasCreated();
    }

    @Test
    @Category({CriticalPriority.class, RegisteredUser.class})
    public void registerUserCanWriteAComment() {
        postPage.registeredUserCommentPost();
        postPage.assertCommentWasCreated();
    }

    @Test
    @Category({CriticalPriority.class, RegisteredUser.class})
    public void registeredUserCanEditHisPost() {
        postPage.registeredUserCanEditHisPost();
        postPage.assertPostWasEdited();
    }

    @Test
    @Category({MediumPriority.class, RegisteredUser.class})
    public void registeredUserCanLikeAndDislikePost() {
        postPage.registeredUserCanLikeOrDislikeUserPost();
        postPage.assertUserDislikedPost();
    }
}
