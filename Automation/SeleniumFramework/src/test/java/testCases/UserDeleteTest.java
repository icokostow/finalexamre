package testCases;

import categoriesForGroupTesting.CriticalPriority;
import categoriesForGroupTesting.RegisteredUser;
import com.telerikacademy.finalproject.pages.PostPage;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;

public class UserDeleteTest extends BaseTest {

    @AfterClass
    public static void makePostForDeletionAndLogOut() {
        PostPage postPage = new PostPage();
        postPage.writePostТоDeletе();
        postPage.writeCommentТоDeletе();
    }

    @After
    public void logOutFromWeAreSocialNetwork() {
        loginPage.logOutFromWeAreSocialNetwork();
    }

    @Test
    @Category({CriticalPriority.class, RegisteredUser.class})
    public void registerUserCanDeleteHisPost() {
        postPage.registeredUserCanDeleteHisPost();
        postPage.assertPostWasDeleted();
    }

    @Test
    @Category({CriticalPriority.class, RegisteredUser.class})
    public void registeredUserCanDeleteHisComment() {
        postPage.registeredUserCanDeleteHisComment();
        postPage.assertCommentWasDeleted();
    }
}
