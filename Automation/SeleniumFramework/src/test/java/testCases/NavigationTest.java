package testCases;

import categoriesForGroupTesting.CriticalPriority;
import categoriesForGroupTesting.NavigationAndLogIn;
import org.junit.Test;
import org.junit.experimental.categories.Category;

public class NavigationTest extends BaseTest{

    @Test
    @Category({CriticalPriority.class, NavigationAndLogIn.class})
    public void userGoToHomePageUsingHomeButton(){
        navigationPage.navigateToPage();
        navigationPage.assertLoginPageNavigated();
    }
}
