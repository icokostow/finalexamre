package testCases;

import categoriesForGroupTesting.CriticalPriority;
import categoriesForGroupTesting.NavigationAndLogIn;
import categoriesForGroupTesting.RegisteredUser;
import com.telerikacademy.finalproject.pages.LoginPage;
import org.junit.After;
import org.junit.Test;
import org.junit.experimental.categories.Category;

public class LoginTest extends BaseTest {

    @After
    public void logOutFromWeAreSocialNetwork() {
        loginPage.logOutFromWeAreSocialNetwork();
    }

    @Test
    @Category({CriticalPriority.class, NavigationAndLogIn.class, RegisteredUser.class})
    public void userLogInWeAreSocialNetwork() {
        loginPage.userCanLogInWeAreSocialNetwork(LoginPage.USERNAME, LoginPage.PASSWORD);
        loginPage.assertUserIsLoggedIn();
    }

}
