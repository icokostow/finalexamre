package testCases;

import categoriesForGroupTesting.HighPriority;
import categoriesForGroupTesting.RegisteredUser;
import org.junit.After;
import org.junit.Test;
import org.junit.experimental.categories.Category;

public class UserTest extends BaseTest {

    @After
    public void logOutFromWeAreSocialNetwork() {
        loginPage.logOutFromWeAreSocialNetwork();
    }

    @Test
    @Category({HighPriority.class, RegisteredUser.class})
    public void userCanEditHisProfileInWeAreSocialNetwork() {
        userPage.userEditHisProfile();
        userPage.assertUserChangedHisProfile();
    }

    @Test
    @Category({HighPriority.class, RegisteredUser.class})
    public void userCanSendAndAcceptConnectionRequestAndDisconnectFromEachOther() {
        userPage.userCanSendAcceptConnectionReqestAndDisconnectFromUser();
        userPage.assertUsersAreDisconnected();
    }

}
