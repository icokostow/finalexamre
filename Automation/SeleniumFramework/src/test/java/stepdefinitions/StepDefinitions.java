package stepdefinitions;

import com.telerikacademy.finalproject.utils.UserActions;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

public class StepDefinitions extends BaseStepDefinitions{
    UserActions actions = new UserActions();

    @Given("Click $element element")
    @When("Click $element element")
    @Then("Click $element element")
    public void clickElement(String element){
        actions.clickElement(element);
    }

    @Given("Click on $btn hidden button")
    @When("Click on $btn hidden button")
    @Then("Click on $btn hidden button")
    public void clickHiddenElement(String btn){
        actions.clickHiddenElement(btn);
    }

    @Given("Type $name in $name field")
    @When("Type $name in $name field")
    @Then("Type $name in $name field")
    public void typeInField(String value, String field){
        actions.typeValueInField(value, field);
    }

    @When("Wait for $element, $value seconds")
    @Then("Wait for $element, $value seconds")
    public void waitForElement(String locator,int timeout){
        actions.waitForElementPresentTillTimeout(locator, timeout);
    }
    @When("Wait for, $value seconds")
    public void wait(int timeout){
        actions.waitFor(timeout);
    }

    @Then("Assert that $element present")
    public void assertForElementPresent(String locator){
        actions.assertElementPresent(locator);
    }

    @Then("Assert that there is no such $element")
    public void assertForElementIsNotPresent(String locator, String text){
        actions.assertElementIsNotPresent(locator, text);
    }

}
