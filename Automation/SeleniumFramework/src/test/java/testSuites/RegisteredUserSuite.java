package testSuites;

import categoriesForGroupTesting.Admin;
import categoriesForGroupTesting.RegisteredUser;
import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import testCases.*;

@RunWith(Categories.class)
@Categories.IncludeCategory(RegisteredUser.class)
@Suite.SuiteClasses ({LoginTest.class, PostTest.class, UserDeleteTest.class
                    ,UserTest.class})

public class RegisteredUserSuite {
}
