package testSuites;

import categoriesForGroupTesting.*;
import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import testCases.*;

@RunWith(Categories.class)
@Categories.IncludeCategory({ Admin.class, CriticalPriority.class, HighPriority.class,
        MediumPriority.class, NavigationAndLogIn.class, RegisteredUser.class})
@Suite.SuiteClasses( { AdminDeleteTest.class, AdminTest.class, LoginTest.class, NavigationTest.class,
        PostTest.class, UnregisteredUserTest.class, UserDeleteTest.class, UserTest.class})

public class AllTestCases {
}
