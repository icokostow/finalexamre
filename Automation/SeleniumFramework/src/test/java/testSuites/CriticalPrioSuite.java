package testSuites;

import categoriesForGroupTesting.CriticalPriority;
import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import testCases.*;

@RunWith(Categories.class)
@Categories.IncludeCategory(CriticalPriority.class)
@Suite.SuiteClasses({AdminDeleteTest.class, AdminTest.class, LoginTest.class, NavigationTest.class,
        PostTest.class, UserDeleteTest.class, })

public class CriticalPrioSuite {
}
