package testSuites;

import categoriesForGroupTesting.Admin;
import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import testCases.*;

@RunWith(Categories.class)
@Categories.IncludeCategory(Admin.class)
@Suite.SuiteClasses ({AdminDeleteTest.class, AdminTest.class})

public class AdminTestSuite {
}
