package testSuites;

import categoriesForGroupTesting.MediumPriority;
import categoriesForGroupTesting.NavigationAndLogIn;
import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import testCases.*;

@RunWith(Categories.class)
@Categories.IncludeCategory(NavigationAndLogIn.class)
@Suite.SuiteClasses ({NavigationTest.class, LoginTest.class})

public class NavigationAndLogInSuite {
}
