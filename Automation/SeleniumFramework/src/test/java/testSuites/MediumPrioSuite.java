package testSuites;

import categoriesForGroupTesting.MediumPriority;
import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import testCases.PostTest;
import testCases.UnregisteredUserTest;
import testCases.UserTest;

@RunWith(Categories.class)
@Categories.IncludeCategory(MediumPriority.class)
@Suite.SuiteClasses ({UnregisteredUserTest.class, PostTest.class})

public class MediumPrioSuite {
}
