package testSuites;

import categoriesForGroupTesting.CriticalPriority;
import categoriesForGroupTesting.HighPriority;
import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import testCases.UnregisteredUserTest;
import testCases.UserTest;

@RunWith(Categories.class)
@Categories.IncludeCategory(HighPriority.class)
@Suite.SuiteClasses ({UnregisteredUserTest.class, UserTest.class})

public class HigPrioSuite {
}
