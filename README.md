# Get to know our project

# Address of our web application

- https://mighty-tundra-55013.herokuapp.com/

# Address of REST API documentation

- https://mighty-tundra-55013.herokuapp.com/swagger-ui.html#

# Link with all project's documents:

https://telerikacademy-my.sharepoint.com/:f:/p/hristo_kostov_a22_learn/Eu9WDDIJOcxPhthA7bxwwesBIreFct_koKzUUj_bJYrSRw

# Link to the project's Test Plan:

- [Test Plan](https://telerikacademy-my.sharepoint.com/:w:/p/hristo_kostov_a22_learn/EbqW9yaFj9dGiZolU47EudABUmECDBbWf2P3Q1dKH5omfg)

- Test Cases, Test Report and Bug Report would be found at the 10. Deliverables section of the Test Plan!

- The test cases a gouped in category - CriticalPrio, HighPrio, MediumPrio, Negative. Besides they are grouped by method of done - Automated or Manual.

- To read a single test scenario go to tab Summary and SummaryAll and click on the name of the test case

# Project Guide #

# I.The repository must be cloned or downloaded

- Refer to  https://git-scm.com/docs/git-clone

# II.Automated tests:

## 1. Selenium automated UI tests. Go to Automation -> SeleniumFramework dir or use the link below: 

https://gitlab.com/icokostow/finalexamre/-/tree/master/Automation/SeleniumFramework

### Open and build the project with a IDE for Java

### The Automated UI tests are grouped by categories:

	- RunCriticalPrioSuite.bat file executes the critical test cases that are most crucial for the successfull release of the web application

	- RunHighPrioSuite.bat file executes the category with high priority cases

	- RunMediumPrioSuite.bat file executes the least important test cases

	- RunAdminTestsSuite.bat file executes admin test cases
	
	- RunRegisteredUserSuite file executes test cases for registered user
	
	- RunUnregisteredUserSuite file executes tes cases for unregistered user

	- RunJBehave file executes JBehave tests

	- RunAllTestCases executes all test cases


## 2. SikuliX automated UI tests. Go to Automation -> SikuliX dir or use the link below: 

https://gitlab.com/icokostow/finalexamre/-/tree/master/Automation/SikuliX

### Read and follow instruction in ReadMe.txt file


## 3. REST API tests 

### Go to Automation -> RestApi dir or use the link below:

https://gitlab.com/icokostow/finalexamre/-/tree/master/Automation/RestApi

	- To use Postman import WeAre.postman_collection.json and WeAre.postman_environment.json

	- To run the collection with newman and receive a report execute RunRestCollection.bat
   !!!! Do not close the local server that has been initiated it is needed for storing data from DB as API can not provide it !!!

	- To see test results from RestApi tests go to Automation -> RestApi -> newman and download and execute WeAre-2020-12-23-06-32-49-689-0.html file

## 4. Load Performance tests

### Prerequisites - you need installed tool JMeter

### Go to Automation -> JmeterTests dir or use the link below:

https://gitlab.com/icokostow/finalexamre/-/tree/master/Automation/JmeterTests

	- import collection WeAreLoadTest.jmx in JMeter tool

	- Just run collection from JMeter tool

